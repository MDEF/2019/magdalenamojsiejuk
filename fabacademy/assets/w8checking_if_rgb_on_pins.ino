//This code checks separate colors in RGB LED to help establish how they are connected to the board
//IT IS MADE FOR COMMON ANODE (+) LED‹


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(11, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(9, HIGH);    // beginning with all turned OFF, to clearly see the colors 
  digitalWrite(11, HIGH);
  digitalWrite(10, HIGH);
  delay(2000);
   
  digitalWrite(9, LOW);   // turn the LED on ON when on LOW 
  delay(2000);                       // wait for two seconds
  digitalWrite(9, HIGH);    // turn the LED OFF when HIGH
  delay(2000);   
  digitalWrite(10, LOW);   
  delay(2000);                     
  digitalWrite(10, HIGH);  
  delay(2000);  
  digitalWrite(11, LOW);   
  delay(2000);                    
  digitalWrite(11, HIGH);    
  delay(2000);        
}
