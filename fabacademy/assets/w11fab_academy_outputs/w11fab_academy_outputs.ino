#include <Servo.h>
// setting up servo
Servo servo;

int servo_position = 0;

//settign up ultrasounds
int trigPin = 11;    // Trigger
int echoPin = 12;    // Echo
long duration, cm, inches;
int LEDr=8;      


void setup() {
  Serial.begin (9600);

  //servo attached to pin 9
  servo.attach (9);
  servo.write(0);

  // sensor pins states
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

 //led pin state
  pinMode(led, OUTPUT);
}

void loop() {

  //copied from sensor code
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);

  cm = (duration / 2) / 29.1;

  if (cm < 20) {
    
    servo.write(40);
  digitalWrite(led, HIGH); 
    
    delay(100);
  }


  else {
    servo.write(90);
    digitalWrite(led, LOW); 
    delay(100);
  }

  Serial.print(cm);
  Serial.print("cm");
  Serial.print(servo_position);
  Serial.println();
  delay(250);

}
